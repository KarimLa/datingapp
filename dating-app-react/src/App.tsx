import React from "react";
import Value from "./components/Value/Value";

const App: React.FC = () => {
  return (
    <>
      <Value />
    </>
  );
};

export default App;
