import React, { useState, useEffect } from "react";

const Value: React.FC = () => {
  const [values, setValues] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const data = await (await fetch(
        "http://localhost:5000/api/values"
      )).json();
      setValues(data);
    };

    fetchData();

    return () => {
      setValues([]);
    };
  }, []);

  console.log(values);
  return (
    <div style={{ textAlign: "center" }}>
      <h1>Welcome to DatingApp</h1>
      {values.length ? (
        values.map(({ id, name }) => <p key={id}>{name}</p>)
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
};

export default Value;
